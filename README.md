# Rwitter
Rwitter is a minimal application that users can post messages and others can see the messages.

### Stack
This project has built with Spring framework based on Java and database platform is MongoDB.

### License
This project is published under MIT license, and you can use this project freely.