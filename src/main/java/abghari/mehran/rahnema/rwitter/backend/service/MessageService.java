package abghari.mehran.rahnema.rwitter.backend.service;

import abghari.mehran.rahnema.rwitter.backend.model.Message;

import java.util.List;

public interface MessageService {
    void addMessage(Message message);
    List<Message> getMessages();
}
