package abghari.mehran.rahnema.rwitter.backend.repository;

import abghari.mehran.rahnema.rwitter.backend.model.Message;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MessageRepository extends MongoRepository<Message, String> {
}
