package abghari.mehran.rahnema.rwitter.backend.domain;

import abghari.mehran.rahnema.rwitter.backend.model.Message;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MessageInput {
  private String message;
  private String user;

  public Message toMessage() {
    return new Message(this.getUser(), this.getMessage());
  }
}
