package abghari.mehran.rahnema.rwitter.backend.controller;

import abghari.mehran.rahnema.rwitter.backend.domain.MessageInput;
import abghari.mehran.rahnema.rwitter.backend.model.Message;
import abghari.mehran.rahnema.rwitter.backend.service.MessageService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class MessageController {
    private final MessageService service;

    public MessageController(MessageService service) {
        this.service = service;
    }

    @PostMapping("messages")
    @ResponseStatus(HttpStatus.CREATED)
    public void addNewMessage(@RequestBody MessageInput message) {
        service.addMessage(message.toMessage());
    }

    @GetMapping("messages")
    public List<Message> getMessages() {
        return service.getMessages();
    }
}
