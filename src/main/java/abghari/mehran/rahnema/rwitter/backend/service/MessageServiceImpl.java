package abghari.mehran.rahnema.rwitter.backend.service;

import abghari.mehran.rahnema.rwitter.backend.model.Message;
import abghari.mehran.rahnema.rwitter.backend.repository.MessageRepository;

import org.springframework.core.annotation.Order;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class MessageServiceImpl implements MessageService {
  private final MessageRepository repository;

  public MessageServiceImpl(MessageRepository repository) {
    this.repository = repository;
  }

  @Override
  public void addMessage(Message message) {
    message.setTimestamp(new Date());
    repository.save(message);
  }

  @Override
  public List<Message> getMessages() {
    return repository.findAll(new Sort(Sort.Direction.DESC, "timestamp"));
  }
}
